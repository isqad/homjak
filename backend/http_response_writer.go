package main

import (
	"net/http"
)

type httpResponseWriter struct {
	http.ResponseWriter
	status int
	length int
}

func (w *httpResponseWriter) Write(b []byte) (int, error) {
	if w.status == 0 {
		w.status = 200
	}
	n, err := w.ResponseWriter.Write(b)
	w.length += n
	return n, err
}

func (w *httpResponseWriter) WriteHeader(status int) {
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}
