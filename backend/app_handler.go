package main

import (
	"net/http"
)

type AppHandler struct {
	*App
	H func(*App, http.ResponseWriter, *http.Request)
}

func (h AppHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	sw := httpResponseWriter{ResponseWriter: w}

	h.withLog(h.H)(&sw, r)
}
