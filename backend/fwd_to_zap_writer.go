package main

import (
	"go.uber.org/zap"
)

type FwdToZapWriter struct {
	Logger *zap.Logger
}

func (fw *FwdToZapWriter) Write(p []byte) (n int, err error) {
	fw.Logger.Error(string(p))
	return len(p), nil
}
