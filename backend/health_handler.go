package main

import (
	"io"
	"net/http"
	"sync/atomic"
)

type HealthCheckHandler struct {
	Healthy int32
}

func (h HealthCheckHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if atomic.LoadInt32(&h.Healthy) == 1 {
		w.Header().Set("X-Content-Type-Options", "nosniff")
		w.Header().Set("X-Content-Type", "application/json; charset=utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		io.WriteString(w, `{"alive": true}`)

		return
	}
	w.WriteHeader(http.StatusServiceUnavailable)
}
