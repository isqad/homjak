package main

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync/atomic"
	"syscall"
	"time"
)

func main() {
	var (
		logger *zap.Logger
		err    error
	)
	if os.Getenv(`APP_ENV`) == `production` {
		logger, err = zap.NewProduction()
	} else {
		logger, err = zap.NewDevelopment()
	}

	if err != nil {
		log.Panicf("Can't initialize zap logger: %v", err)
	}

	a := NewApp(logger)

	router := mux.NewRouter()
	healthHandler := HealthCheckHandler{Healthy: 0}
	atomic.StoreInt32(&healthHandler.Healthy, 1)
	router.Handle("/ping", healthHandler).
		Methods("GET")

	listenAddr := os.Getenv(`HOMJAK_LISTEN_ADDR`)
	if len(listenAddr) == 0 {
		listenAddr = ":3001"
	}
	httpServer := &http.Server{
		Addr:         listenAddr,
		Handler:      router,
		ErrorLog:     log.New(&FwdToZapWriter{a.Log}, "", 0),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-quit
		httpServer.ErrorLog.Println("Shutdown signal received...")
		atomic.StoreInt32(&healthHandler.Healthy, 0)

		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		httpServer.SetKeepAlivesEnabled(false)
		if err := httpServer.Shutdown(ctx); err != nil {
			httpServer.ErrorLog.Fatal("Could not gracefully shutdown the server", zap.Error(err))
		}

		a.Log.Sync()

		close(done)
	}()

	a.Log.Debug("Starting http server...")

	if err := httpServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		a.Log.Fatal(fmt.Sprintf("Could not listen on %s: %v\n", listenAddr, err))
	}

	<-done
	httpServer.ErrorLog.Println("Server gracefully stopped.")
}
