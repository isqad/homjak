package main

import (
	"go.uber.org/zap"
	"net/http"
	"strings"
	"time"
)

type App struct {
	Log *zap.Logger
}

func NewApp(logger *zap.Logger) *App {
	return &App{Log: logger}
}

func (ctx *App) withLog(next func(*App, http.ResponseWriter, *http.Request)) func(*httpResponseWriter, *http.Request) {
	return func(w *httpResponseWriter, r *http.Request) {
		start := time.Now()
		log := ctx.Log

		next(ctx, w, r)

		duration := time.Now().Sub(start)

		log.Info(r.Method+" "+r.URL.Path,
			zap.String(`method`, r.Method),
			zap.String(`clientip`, ctx.IpAddr(r)),
			zap.String(`user_agent`, r.UserAgent()),
			zap.Int(`status`, w.status),
			zap.Int(`content_len`, w.length),
			zap.Int64(`duration`, int64(duration)),
		)
	}
}

func (ctx *App) IpAddr(r *http.Request) string {
	var (
		ip            string
		xForwardedFor = http.CanonicalHeaderKey("X-Forwarded-For")
		xRealIP       = http.CanonicalHeaderKey("X-Real-IP")
	)

	if xff := r.Header.Get(xForwardedFor); xff != "" {
		i := strings.Index(xff, ", ")
		if i == -1 {
			i = len(xff)
		}
		ip = xff[:i]
	} else if xrip := r.Header.Get(xRealIP); xrip != "" {
		ip = xrip
	}

	return ip
}
