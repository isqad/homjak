module gitlab.com/isqad/homjak

go 1.12

require (
	github.com/gorilla/mux v1.7.1
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
)
